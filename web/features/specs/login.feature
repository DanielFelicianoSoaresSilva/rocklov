#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da RockLov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "papito@yahoo.com" e "pwd123"
        Então sou redirecionado para o Dashboard

    @tentar_logar
    Esquema do Cenario: Tentar logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input      | senha_input | mensagem_output                  |
            | daniel@gmail.com | 123pw       | Usuário e/ou senha inválidos.    |
            | dan@gmail.com    | pwd123      | Usuário e/ou senha inválidos.    |
            | daniel!gmail.com | pwd123      | Oops. Informe um email válido!   |
            |                  | pwd123      | Oops. Informe um email válido!   |
            | daniel@gmail.com |             | Oops. Informe sua senha secreta! |
