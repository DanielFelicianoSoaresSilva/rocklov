require_relative "base_api"

class Sessions < BaseApi

  def login(payload)
    # payload = { email: email, password: pass } foi para o post_session
    return self.class.post(
             "/sessions",
             body: payload.to_json,
             headers: { "Content-Type": "application/json" },
           )
  end
end
